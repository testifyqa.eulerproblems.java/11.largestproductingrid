package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PinkFlamingoTest {

    private PinkFlamingo pinkFlamingo = new PinkFlamingo();

    @Test
    void testFizzBuzz() {
        String expected = "0,1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,Fizz Buzz";
        String actual = pinkFlamingo.solution(0, 15);

        assertEquals(expected, actual);
    }

    @Test
    void testPinkFlamingo() {
        String expected = "6761,Fizz,6763,6764,Pink Flamingo";
        String actual = pinkFlamingo.solution(6761, 6765);

        assertEquals(expected, actual);
    }
}