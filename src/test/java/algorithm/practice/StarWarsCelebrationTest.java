package algorithm.practice;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class StarWarsCelebrationTest {

    private StarWarsCelebration starWarsCelebration = new StarWarsCelebration();

    @Test
    public void test() throws IOException {
        assertEquals(6, starWarsCelebration.run("Darth Vader"));
    }

}