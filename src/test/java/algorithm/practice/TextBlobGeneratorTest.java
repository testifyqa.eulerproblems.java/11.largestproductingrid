package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TextBlobGeneratorTest {

    private TextBlobGenerator.DataWrapper dataWrapper = new TextBlobGenerator.DataWrapper();
    private TextBlobGenerator generator;

    @Test
    void testTextBlobGenerator_LeftPadding() {
        dataWrapper.instruction = "addLettersFor:9-loops,padLeftFor:99-loops,padRightFor:999-loops,paddingStyle:left";
        generator = new TextBlobGenerator(dataWrapper);

        String expected = "---------------------------------------------------------------------------------------------------abcdefabc";
        String actual = generator.getBlob();
        assertEquals(expected, actual);
    }

    @Test
    void testTextBlobGenerator_RightPadding() {
        dataWrapper.instruction = "addLettersFor:23-loops,padLeftFor:111-loops,padRightFor:52-loops,paddingStyle:right";
        generator = new TextBlobGenerator(dataWrapper);

        String expected = "abcdefabcdefabcdefabcde----------------------------------------------------";
        String actual = generator.getBlob();
        assertEquals(expected, actual);
    }

    @Test
    void testTextBlobGenerator_NoPadding() {
        dataWrapper.instruction = "addLettersFor:10-loops,padLeftFor:17-loops,padRightFor:12-loops";
        generator = new TextBlobGenerator(dataWrapper);

        String expected = "abcdefabcd";
        String actual = generator.getBlob();
        assertEquals(expected, actual);
    }

    @Test
    void testTextBlobGenerator_NegativeNumbersGetReadAsPositive() {
        dataWrapper.instruction = "addLettersFor:-6-loops,padLeftFor:-3-loops,padRightFor:-2-loops,paddingStyle=left";
        generator = new TextBlobGenerator(dataWrapper);

        String expected = "---abcdef";
        String actual = generator.getBlob();
        assertEquals(expected, actual);
    }

}