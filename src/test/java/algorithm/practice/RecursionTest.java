package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecursionTest {

    private Recursion recursion = new Recursion();

    @Test
    void testFactorial_CorrectResult() {
        assertEquals(120, recursion.factorial(5));
    }
}