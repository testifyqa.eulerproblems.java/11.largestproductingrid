package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromeTest {

    private Palindrome palindrome = new Palindrome();

    @Test
    void testCheckIfPalindrome_True() {
        assertTrue(palindrome.checkIfPalindrome("madam"));
    }

    @Test
    void testCheckIfPalindrome_False() {
        assertFalse(palindrome.checkIfPalindrome("truck"));
    }
}