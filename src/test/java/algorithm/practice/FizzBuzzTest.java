package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    @Test
    public void test() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("1,2,Fizz,4,Buzz", fizzBuzz.run(1,5));
    }

}