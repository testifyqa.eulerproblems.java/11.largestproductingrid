package algorithm.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanNumeralsTest {

    private RomanNumerals romanNumerals = new RomanNumerals();

    @Test
    void testInttoRoman() {
        assertEquals("MI", romanNumerals.toRoman(1001));
        assertEquals("VII", romanNumerals.toRoman(7));
    }
}