package algorithm.practice;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    private BinarySearch binarySearch = new BinarySearch();

    @Test
    void testRunBinarySearchIteratively_CorrectResult() {
        assertEquals(4, binarySearch.runBinarySearchIteratively(new int[]{2,3,4,5,6,7}, 6, 0, 5));
    }

    @Test
    void testRunBinarySearchRecursively_CorrectResult() {
        assertEquals(2, binarySearch.runBinarySearchRecursively(new int[]{2,3,4,5,6,7}, 4, 0, 5));
    }

    @Test
    void testRunBinarySearchusingArraysClass_CorrectResult() {
        assertEquals(3, binarySearch.runBinarySearchUsingArraysClass(new int[]{2,3,4,5,6,7}, 5));
    }

    @Test
    void runBinarySearchUsingCollectionsClass() {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);

        assertEquals(5, binarySearch.runBinarySearchUsingCollectionsClass(list, 7));
    }
}