package euler.problems;

import euler.problems.Euler11_LargestProductInGrid;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Euler11LargestProductInGridTest {

    @Test
    void testLargestProductInGrid_CorrectResult() {
        Euler11_LargestProductInGrid euler11LargestProductInGrid = new Euler11_LargestProductInGrid();

        assertEquals(51267216, euler11LargestProductInGrid.getLargest());
    }
}
