package euler.problems;

import euler.problems.Euler12_HighlyDivisibleTriangularNumber;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Euler12HighlyDivisibleTriangularNumberTest {

    @Test
    void testDivisorsForTriangularNumber_CorrectNumberOfDivisors() {
        Euler12_HighlyDivisibleTriangularNumber euler12HighlyDivisibleTriangularNumber = new Euler12_HighlyDivisibleTriangularNumber();
        euler12HighlyDivisibleTriangularNumber.getDivisorsOfTriangularNumber(4);


        assertEquals(21, euler12HighlyDivisibleTriangularNumber.getDivisorsOfTriangularNumber(4));
        assertEquals(28, euler12HighlyDivisibleTriangularNumber.getDivisorsOfTriangularNumber(6));
        assertEquals(76564125, euler12HighlyDivisibleTriangularNumber.getDivisorsOfTriangularNumber(500));
    }

}