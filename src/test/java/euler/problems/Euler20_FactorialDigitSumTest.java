package euler.problems;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class Euler20_FactorialDigitSumTest {

    private Euler20_FactorialDigitSum factorialDigitSum = new Euler20_FactorialDigitSum();

    @Test
    void testSumOfFactorial_CorrectResult() {
        assertEquals(8232, factorialDigitSum.sumOfFactorial(100));
    }

    @Test
    void testSumOfFactorial_StackOverflowError() throws StackOverflowError {
        assertThrows(StackOverflowError.class, () -> {
            assertEquals(-8232, factorialDigitSum.sumOfFactorial(-100));
        });
    }
}