package euler.problems;

import euler.problems.Euler13_LargeSum;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class Euler13LargeSumTest {

    private Euler13_LargeSum euler13LargeSum = new Euler13_LargeSum();

    @Test
    void testFirstTenDigitsOfSum_IsCorrect() throws IOException {
        assertEquals(5537376230L, euler13LargeSum.firstTenDigitsOfSum());
    }

    @Test
    void shouldThrowAnException() {
        assertThrows(AssertionFailedError.class, () -> {
           assertEquals("bad string", euler13LargeSum.firstTenDigitsOfSum());
           assertEquals(1234567890L, euler13LargeSum.firstTenDigitsOfSum());
        });
    }
}