package euler.problems;

import euler.problems.Euler15_LatticePaths;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class Euler15LatticePathsTest {

    private Euler15_LatticePaths euler15LatticePaths = new Euler15_LatticePaths();

    @Test
    void testGetNumOfRoutesInGridOfSize_CorrectNumberOfRoutes() {
        assertEquals(BigInteger.valueOf(6), euler15LatticePaths.getNumOfRoutesInGridOfSize(new int[2][2]));
        assertEquals(BigInteger.valueOf(137846528820L), euler15LatticePaths.getNumOfRoutesInGridOfSize(new int[20][20]));
    }
}