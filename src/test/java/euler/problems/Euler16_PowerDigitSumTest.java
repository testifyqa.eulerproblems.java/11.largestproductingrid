package euler.problems;

import euler.problems.Euler16_PowerDigitSum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Euler16_PowerDigitSumTest {

    private Euler16_PowerDigitSum powerDigitSum = new Euler16_PowerDigitSum();

    @Test
    void testGetSumOfDigitsInResult_CorrectSumResult() {
        assertEquals(26, powerDigitSum.getSumOfDigitsInResult(2, 15));
        assertEquals(88, powerDigitSum.getSumOfDigitsInResult(2, 1000));
    }
}