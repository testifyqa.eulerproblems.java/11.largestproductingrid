package euler.problems;

import euler.problems.Euler14_LongestCollatzSequence;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Euler14LongestCollatzSequenceTest {

    private Euler14_LongestCollatzSequence euler14LongestCollatzSequence = new Euler14_LongestCollatzSequence();

    @Test
    void testStartingNumberWithLongestChainInCollatzSequence_IsCorrect() {
        assertEquals(910107, euler14LongestCollatzSequence.startingNumberWithLongestChainInCollatzSequence());
    }
}