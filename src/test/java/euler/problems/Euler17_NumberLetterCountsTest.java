package euler.problems;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Euler17_NumberLetterCountsTest {

    private Euler17_NumberLetterCounts numberLetterCounts = new Euler17_NumberLetterCounts();

    @Test
    void testLetterCount_CorrectCount() {
        assertEquals(19, numberLetterCounts.letterCount(5));
        assertEquals(21124, numberLetterCounts.letterCount(1000));
    }
}