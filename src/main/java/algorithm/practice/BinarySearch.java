package algorithm.practice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class BinarySearch {

    //Iterative Implementation
    int runBinarySearchIteratively(int[] sortedArray, int key, int low, int high) {
        int index = 0;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (sortedArray[mid] < key) {
                low = mid + 1;
            }
            else if (sortedArray[mid] > key) {
                high = mid - 1;
            }
            else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
        return index;


    }

    //Recursive implementation
    int runBinarySearchRecursively(int[] sortedArray, int key, int low, int high) {
        int mid = (low + high) / 2;

        if (high < low) return -1;

        if (key == sortedArray[mid]) {
            return mid;
        }
        else if (key < sortedArray[mid]) {
            return runBinarySearchRecursively(sortedArray, key, low, mid-1);
        }
        else {
            return runBinarySearchRecursively(sortedArray, key, mid+1, high);
        }
    }

    //Using Arrays.binarySearch()
    int runBinarySearchUsingArraysClass(int[] sortedArray, int key) {
        return Arrays.binarySearch(sortedArray, key);
    }

    //Using Collections.binarySearch()
    int runBinarySearchUsingCollectionsClass(List<Integer> sortedArray, int key) {
        return Collections.binarySearch(sortedArray, key);
    }
}
