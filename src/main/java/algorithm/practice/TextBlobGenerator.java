package algorithm.practice;


/**
 * Text blob generator takes an instruction string
 *
 * Generates a blob of text using the characters a to f, (should loop back to a if number of letters to be generated is more than 6)
 *
 * Then pads the blob with dashes to either the left or right depending on the padding style,
 *
 * or no padding if no padding style defined
 *
 * eg. if input instruction string:
 * 		addLettersFor:3-loops,padLeftFor:5-loops,padRightFor:6-loops,paddingStyle:left
 * output should be:
 * 		-----abc
 *
 * eg. if input instruction string:
 * 		addLettersFor:8-loops,padLeftFor:5-loops,padRightFor:7-loops,paddingStyle:right
 * output should be:
 * 		abcdefab-------
 *
 * eg. if input instruction string:
 * 		addLettersFor:8-loops,padLeftFor:5-loops,padRightFor:7-loops
 * output should be:
 * 		abcdefab
 *
 * 1) fix the compile issues and run the main method
 *
 * 2) correct any mistakes in logic
 *
 * 3) Make the code cleaner (refactor and simplify classes)
 *
 * 4) test that it works as intended
 */
public class TextBlobGenerator {

    TextBlobGenerator(DataWrapper dataWrapper) {
        String instructions = dataWrapper.getInstruction();
        int addLettersFor = extractNumberFromString(instructions, 0);
        int padLeftFor = extractNumberFromString(instructions, 1);
        int padRightFor = extractNumberFromString(instructions, 2);

        generateBlob(instructions, padLeftFor, addLettersFor, padRightFor);
    }

    private String paddingStyle = null;
    private final StringBuilder blob = new StringBuilder();
    private String[] letters = {"a", "b", "c", "d", "e", "f"};

    private void generateBlob(String instructions, int padLeftFor, int addLettersFor, int padRightFor) {

        try {
            paddingStyle = instructions.split(",")[3];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("no style has been defined - exception thrown: " + e);
        }

        if (paddingStyle != null && paddingStyle.contains("left")) {
            for (int i = 0; i < padLeftFor; i++) {
                blob.append("-");
            }
            addLettersToBlob(addLettersFor);
        }
        else if (paddingStyle != null && paddingStyle.contains("right")) {
            addLettersToBlob(addLettersFor);
            for (int i = 0; i < padRightFor; i++) {
                blob.append("-");
            }
        }
        else if (paddingStyle == null) {
            addLettersToBlob(addLettersFor);
        }
    }

    private int extractNumberFromString(String string, int index) {
        return Integer.parseInt(string.split(",")[index].replaceAll("[^0-9]", ""));
    }

    private String addLettersToBlob(int addLettersFor) {
        for (int i = 0; i < addLettersFor; i++) {
            if (i >= letters.length)  {
                blob.append(letters[i % letters.length]);
            }
            else blob.append(letters[i]);
        }
        return blob.toString();
    }

    String getBlob() {
        return blob.toString();
    }

    public static class DataWrapper {
        String instruction;

        String getInstruction() {
            return instruction;
        }

        public String getInst() {
            return instruction;
        }

        /**
         * Equal if and ONLY if both objects contain the same string instruction
         */
        @Override
        public boolean equals(Object other){
            return this == other;
        }
    }

    public static void main(String[] args) {
        DataWrapper dataWrapper = new DataWrapper();
        dataWrapper.instruction = "addLettersFor:9-loops,padLeftFor:99-loops,padRightFor:999-loops,paddingStyle:left";

        System.out.println(new TextBlobGenerator(dataWrapper).getBlob());
    }
}