package algorithm.practice;

public class Recursion {

    long factorial(int number) {
        if (number == 1) return 1;
        return number * factorial(number - 1);
    }
}
