package algorithm.practice;

public class BlindBinarySearch {

    //Iterative implementation
    int iterativeBinarySearch(int[] sortedArray, int key, int low, int high) {
        int index = 0;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (sortedArray[mid] < key) {
                low = mid + 1;
            }
            else if (sortedArray[mid] > key) {
                high = mid - 1;
            }
            else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
        return index;
    }


    //Recursive implementation
    int recursiveBinarySearch(int[] sortedArray, int key, int low, int high) {
        int mid = (low + high) / 2;

        if (high < low) return -1;

        if (sortedArray[mid] == key) {
            return mid;
        }
        else if (sortedArray[mid] < key) {
            return recursiveBinarySearch(sortedArray, key, mid+1, high);
        }
        else  {
            return recursiveBinarySearch(sortedArray, key, low, high - 1);
        }
    }


}
