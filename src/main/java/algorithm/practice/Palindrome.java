package algorithm.practice;

class Palindrome {

    boolean checkIfPalindrome(String word) {
        String wordReversed = new StringBuilder(word).reverse().toString();
        return word.equals(wordReversed);
    }
}
