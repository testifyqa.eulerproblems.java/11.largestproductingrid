package algorithm.practice;

class FizzBuzz {

    String run(int N, int M) {
        String sequence = "";

        for(int i = N; i<=M; i++) {
            if (i%3==0 && i%5==0) sequence += "FizzBuzz";
            else if (i%3==0) sequence += "Fizz";
            else if (i%5==0) sequence += "Buzz";
            else sequence += i;
            sequence += ",";
        }


        return sequence.substring(0, sequence.length()-1);
    }
}
