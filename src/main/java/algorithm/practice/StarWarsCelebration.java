package algorithm.practice;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class StarWarsCelebration {

    public int run(String character) {
        /*
         * Some work here; return type and arguments should be according to the problem's requirements
         */
        int numberOfFilms = 0;

        String baseUrl = "https://challenges.hackajob.co/swapi/api/people";

        String usedCharacter = character.replaceAll(" ", "%20");

        try {
            URL url = new URL(baseUrl + "/.json?search=" + usedCharacter);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.connect();
            InputStream inputStream = con.getInputStream();
            String json = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
            String films = json.substring(json.indexOf("films"), json.indexOf(']'));
            String[] filmsArray = films.split(",");

            numberOfFilms = filmsArray.length;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return numberOfFilms;
    }
}
