package euler.problems;

public class Euler14_LongestCollatzSequence {

    public int startingNumberWithLongestChainInCollatzSequence() {
        int longestCount = 0;
        int startingNumber = 0;

        int i;
        for (i = 2; i < 1000000; i++) {
            if (longestCount < numberOfTermsInCollatzSequence(i)) {
                longestCount = numberOfTermsInCollatzSequence(i);
                startingNumber = i;
            }
        }
        return startingNumber;
    }

    private int numberOfTermsInCollatzSequence(int number) {
        int count = 0;

        while (number > 1) {
            if (number%2 == 0) {
                number = numberIsEven(number);
            }
            else {
                number = numberIsOdd(number);
            }
            count++;
        }
        return count;
    }

    private int numberIsEven(int number) {
        return number / 2;
    }

    private int numberIsOdd(int number) {
        return (3 * number) + 1;
    }
}
