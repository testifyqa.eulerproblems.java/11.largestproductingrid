package euler.problems;

class Euler12_HighlyDivisibleTriangularNumber {

    private int findAllDivisorsOf(int number) {
        int counter = 0;

        for (int i = 1; i <= (int) Math.sqrt(number); i++) {
            if (number%i == 0) {
                counter += (number % i == i ?  1 : 2);
            }
        }
        return counter;
    }

    int getTriangularNumber(int number) {
        return number * (number+1) / 2;
    }

    public int getDivisorsOfTriangularNumber(int divisorsWanted) {
        int result = 0;
        for (int i = 0; findAllDivisorsOf(getTriangularNumber(i)) <= divisorsWanted; i++) {
            System.out.println(getTriangularNumber(i));
            result = getTriangularNumber(i);
        }
        return result;
    }
}

