package euler.problems;

public class Euler17_NumberLetterCounts {

    public int letterCount(int maxNumber) {
        int sum = 0;
        for (int i = 1; i <= maxNumber; i++) {
            sum = sum + convertNumberToWords(i).length();
        }
        return sum;
    }

    private String convertNumberToWords(int number) {
        String strNumber = null;
        if (0 <= number && number < 20) {
            strNumber = ones()[number];
        }
        else if (20 <= number && number < 100) {
            strNumber = tens()[(number/10)] + (number%10 != 0 ? ones()[number%10] : "");
        }
        else if (100 <= number && number < 1000) {
            strNumber = ones()[number / 100] + "hundred" + (number % 100 != 0 ? "and" + convertNumberToWords(number % 100) : "");
        }
        else if (1000 <= number && number < 10000) {
            strNumber = thousand();
        }

        return strNumber;
    }

    private String[] ones() {
        String[] onesArray = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
                "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        return onesArray;
    }

    private String[] tens() {
        String[] tensArray = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        return tensArray;
    }

    private String thousand() {
        return "onethousand";
    }
}
