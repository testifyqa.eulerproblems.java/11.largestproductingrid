package euler.problems;

import java.math.BigInteger;

public class Euler16_PowerDigitSum {

    int getSumOfDigitsInResult(int number, int toPowerOf) {
        String strResult = getResultOfNumberToPowerOf(number, toPowerOf).toString();
        char[] digits = strResult.toCharArray();

        int sum = 0;

        for (char digit : digits) {
            sum += Character.getNumericValue(digit);
        }

        return sum;
    }

    private BigInteger getResultOfNumberToPowerOf(int number, int toPowerOf) {
        return BigInteger.valueOf((long) Math.pow(number, toPowerOf));
    }
}
