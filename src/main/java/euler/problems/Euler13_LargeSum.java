package euler.problems;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

public class Euler13_LargeSum {

    long firstTenDigitsOfSum() throws IOException {
        BigInteger sum = sumOfAll50DigitArrayValues();
        String strSum = sum.toString();
        String strSumFirstTenDigits = strSum.substring(0, 10);
        return Long.parseLong(strSumFirstTenDigits);
    }

    private BigInteger sumOfAll50DigitArrayValues() throws IOException {
        BigInteger sum = BigInteger.valueOf(0);

        for (String arrayValue : makeArrayOf50DigitNumbers()) {
            BigInteger bigIntArrayValue = new BigInteger(arrayValue);
            sum = sum.add(bigIntArrayValue);
        }
        return sum;
    }

    private String[] makeArrayOf50DigitNumbers() throws IOException {
        String bigNumber = readInBigNumber("src/bigNumber.txt");
        return bigNumber.split("\n");
    }

    private String readInBigNumber(String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

        try {
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
                line = bufferedReader.readLine();
            }
            return stringBuilder.toString();
        } finally {
            bufferedReader.close();
        }
    }
}
