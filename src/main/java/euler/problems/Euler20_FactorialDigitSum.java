package euler.problems;

import java.math.BigInteger;

public class Euler20_FactorialDigitSum {

    private BigInteger factorial(int number) {
        if (number == 1) return BigInteger.ONE;
        return BigInteger.valueOf(number).multiply(factorial(number - 1));
    }

    int sumOfFactorial(int number) {
        int sum = 0;
        char[] digits = factorial(number).toString().toCharArray();
        for (char digit : digits) {
            sum += digit;
        }
        return sum;
    }
}
