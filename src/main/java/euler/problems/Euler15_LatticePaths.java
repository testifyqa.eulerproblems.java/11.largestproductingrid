package euler.problems;

import java.math.BigInteger;

public class Euler15_LatticePaths {

    public BigInteger getNumOfRoutesInGridOfSize(int[][] matrix) {
        int colLength = matrix.length;
        int rowLength = matrix[0].length;
        int matrixLengthTotal = colLength + rowLength;

        BigInteger factorialLengthsTotal = getFactorialOf(matrixLengthTotal);
        BigInteger factorialColArrLength = getFactorialOf(colLength);
        BigInteger factorialRowArrLength = getFactorialOf(rowLength);

        return (factorialLengthsTotal).divide(factorialColArrLength).divide(factorialRowArrLength);
    }

    private BigInteger getFactorialOf(int number) {
        BigInteger factorial = BigInteger.valueOf(1);

        for (int i = 1; i <= number; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}
